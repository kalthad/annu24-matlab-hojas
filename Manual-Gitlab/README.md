- [Información de Mathworks](#org301cf07)
- [Los pasos necesarios](#org57d7f62)
  - [Crear una cuenta en gitlab (con el email de la UCM).](#org2679f7c)
  - [Usar el enlace en el CV para ir al repositorio](#org6019016)
  - [Hacer un fork](#orgdb11df5)
  - [Cómo actualizar vuestro repositorio (Fork)](#orgfbb9def)
    - [Situación](#orgfb18ce2)
  - [Desproteger el branch default.](#orgc00bed6)
    - [Ir a Settings&#x2013;> repository](#org20567e0)
    - [Ir a protected branch&#x2013;>expand](#org6da8601)
    - [Hacer click en unprotect](#org6c42f38)
  - [Activar email Notificación](#org1d39976)
    - [Pasos:](#org0cff22a)
      - [En project selecciona Settings&#x2013;>Integrations&#x2013;>Email on push](#org7f354f1)
      - [Añadir emails in el campo recipentes](#org0b54e57)
  - [Por si acaso, invitar a los  profesores como Developer/maintainer para que pueda clonar, cambiar cosas y hacer un push.](#org47e7d53)
    - [Maintainer o Developer](#orge3de7ff)
    - [Configuración](#orgbec06d0)
  - [Hacer un clon con matlab a una carpeta local en el PC (aula o casa).](#orge3d5ace)
    - [Elegir clonar (opción https)](#org1b1aeb7)
    - [Abrir matlab](#org5113032)
  - [Configuramos nombre y email](#orga68519d)
    - [En el ordenador en los laboratorios de la UCM (cambio local)](#orga485593)
    - [En el ordenador/portátil    individual (cambio global)](#org6fe09c2)
  - [Cambiar, editar ficheros .m con matlab. ¡OJO!](#org8ab1690)
  - [Después de haber terminado el trabajo: hacer un commit con un mensaje «inteligente»](#org45d46ea)
  - [Comprobamos el grafo](#org322b18d)
  - [Qué hacer si `!git` falla](#org6ebf89d)
  - [Hacer un push a su fork en gitlab](#org6b69e95)
  - [Comprobamos con el navegador si todo está bien](#org2f09f43)
  - [Miramos en commits (abajo la project ID)](#orgd1f1da9)
  - [Resolver los problemas con error 403 en la conexión](#orgb68df1a)
- [Como usar Git en clase durante el curso](#org72f93da)



<a id="org301cf07"></a>

# Información de Mathworks

Se puede encontrar información adicional en [Como usar git en Matlab](https://de.mathworks.com/help/matlab/matlab_prog/set-up-git-source-control.html)

Es importante resaltar que, a partir de la versión **2020b**, git está incluido en Matlab y no hace falta instalar nada más.


<a id="org57d7f62"></a>

# Los pasos necesarios

Los pasos para usarlo en nuestra asignatura son los siguientes.

1.  Crear una cuenta en gitlab (con el email de la UCM).
2.  Usar el enlace en el CV para ir al repositorio. Solo hay un repositorio para todas las hojas.
3.  Inicialmente, el repositorio solo contendrá la primera página en un directorio; en las próximas semanas, agregaré las demás páginas. Cada página estará en su propio directorio. Para ello, debéis actualizar vuestro fork de vez en cuando (Vee [fork](#fork))
4.  Hacer un fork.
5.  Desproteger el branch default.
6.  Hacer un clon con matlab a un carpeta local en el PC (aula o casa).
7.  Cambiar, editar ficheros .m con matlab. ¡OJO!
8.  Después de haber terminado el trabajo: hacer un commit con un mensaje «inteligente»
9.  Comprobar los commits, si hace falta cambiamos el nombre y el email
10. Cambiar el nombre y el email
11. Hacer un push a su fork en gitlab
12. Comprobamos con el navegador si todo está bien
13. Miramos en commits
14. En caso de dudas, invitar al profesor como Maintainer para que él pueda clonar, cambiar cosas y hacer un push.


<a id="org2679f7c"></a>

## Crear una cuenta en gitlab (con el email de la UCM).

Vamos a

![img](./images/git-matlab1.png "La página de gilab")

**No usar** sign in with&#x2026; sino *register now*.


<a id="org6019016"></a>

## Usar el enlace en el CV para ir al repositorio

Ir por ejemplo a [Hoja1](https://gitlab.com/kalthad/matlab-hoja1)

![img](./images/git-matlab2.png "Ir la hoja 1")


<a id="orgdb11df5"></a>

## Hacer un fork

Hacer click a la derecha arriba y sale

![img](./images/git-matlab3.png "Hacer un fork")

**OJO**

1.  elegir repositorio *private*
2.  elegir un namespace, tu usuario

![img](./images/git-matlab4.png "Elegir private")

Resultado

![img](./images/git-matlab5.png "elegir usuario")


<a id="orgfbb9def"></a>

## Cómo actualizar vuestro repositorio (Fork)

<a name="fork"></a>


<a id="orgfb18ce2"></a>

### Situación

En el primer día, en el repositorio del cual hacéis un «Fork», solo se encontrarán las hojas de ejercicios nº 1 en un directorio. Después de una semana, agregaré las hojas de ejercicios nº 2 y luego haré push (etc, etc).

Entonces, en vuestro Fork, veréis lo siguiente:

![img](./images/fork-gitlab.png "Actualizar el Fork")

Por favor, debéis pulsar el botón resaltado.


<a id="orgc00bed6"></a>

## Desproteger el branch default.


<a id="org20567e0"></a>

### Ir a Settings&#x2013;> repository

![img](./images/git-matlab6.png "Desproteger el branch default.")


<a id="org6da8601"></a>

### Ir a protected branch&#x2013;>expand

![img](./images/git-matlab7.png "Desproteger el branch default (2)")


<a id="org6c42f38"></a>

### Hacer click en unprotect

![img](./images/git-matlab8.png "Desproteger el branch default (3)")

Y

![img](./images/git-matlab9.png "Desproteger el branch default (4)")


<a id="org1d39976"></a>

## Activar email Notificación

Para minimizar el riesgo de tener [«conflictos de edición»](https://en.wikipedia.org/wiki/Edit_conflict), es mejor activar la notificación por email para cada «push»


<a id="org0cff22a"></a>

### Pasos:


<a id="org7f354f1"></a>

#### En project selecciona Settings&#x2013;>Integrations&#x2013;>Email on push

![img](./images/git-matlab48.png "Email on push")


<a id="org0b54e57"></a>

#### Añadir emails in el campo recipentes

![img](./images/git-matlab49.png "Añadir nuevas direcciones en el campo")

Es importante que en la lista de correos electrónicos se incluyan las siguientes personas:

1.  Tú mismo (para que te des cuenta cuando Laura, Enrique o yo hagamos un push)
2.  Nosotros:
    1.  Laura: Laura Castilla Castellano <laurca04@ucm.es>
    2.  Enrique: Enrique García Sánchez <enriga06@ucm.es>
    3.  Yo: Uwe Brauer <oub@mat.ucm.es>


<a id="org47e7d53"></a>

## Por si acaso, invitar a los  profesores como Developer/maintainer para que pueda clonar, cambiar cosas y hacer un push.


<a id="orge3de7ff"></a>

### Maintainer o Developer

Podéis encontrar detalles en <https://docs.gitlab.com/ee/user/permissions.html>.

Resumen:

1.  Un developer puede hacer un push.
2.  Un maintainer puede hacer un push y realizar cambios en la configuración, por ejemplo modificar las notificaciones, etc.

Dejo a vuestra elección lo que prefiráis.


<a id="orgbec06d0"></a>

### Configuración

Debes incluir

1.  Laura: Laura Castilla Castellano <laurca04@ucm.es>
2.  Enrique: Enrique García Sánchez <enriga06@ucm.es>
3.  Yo: Uwe Brauer <oub@mat.ucm.es>

![img](./images/git-matlab30.png "Invitar el profesor como developer")

y

![img](./images/git-matlab31.png "Invitar el profesor como developer (2)")

Usamos *invite members* a la derecha

![img](./images/git-matlab32.png "Elegimos el profesor")

Usuario es kalthad

Role es developer

![img](./images/git-matlab33.png "Profesor elegido")

y

![img](./images/git-matlab35.png "Resultado de la invitación")

Terminado


<a id="orge3d5ace"></a>

## Hacer un clon con matlab a una carpeta local en el PC (aula o casa).


<a id="org1b1aeb7"></a>

### Elegir clonar (opción https)

![img](./images/git-matlab10.png "Elegir clonar (opción https)")


<a id="org5113032"></a>

### Abrir matlab

![img](./images/git-matlab11.png "Abrir matlab")

Poner el ratón sobre el lado izquierdo blanco, no sobre los nombres de los directorios, y usar el botón de la derecha, sale **source control** y *manage files*

![img](./images/git-matlab12.png "Poner el ratón sobre el lado izquierdo blanco")

1.  en repositorio pones el enlace
2.  y en sandbox eliges una carpeta, por ejemplo matlab-hoja1

![img](./images/git-matlab12.png)

![img](./images/git-matlab13.png "Pon enlace")

![img](./images/git-matlab14.png "Pon carpeta")

![img](./images/git-matlab15.png "Opciones eligidas")

y usas **retrieve**.

Sale usuario y password

![img](./images/git-matlab16.png "pass word y usuario")

Después de haberlo metido, sale

![img](./images/git-matlab17.png "Resultado del clonar")

¡OJO!: todos los ficheros están en verde, quiere decir, todos los cambios están registrados!


<a id="orga68519d"></a>

## Configuramos nombre y email


<a id="orga485593"></a>

### En el ordenador en los laboratorios de la UCM (cambio local)

Cambiamos nombre y email si hace falta usando el Command window (de matlab!). Elegimos la command window. Comprobamos que estamos en la carpeta correcta con el comando

`pwd` y vemos

![img](./images/git-matlab40.png "Carpeta")

Usamos ahora en la **command window**

1.  `!git configure --local user.name "Uwe Brauer"`
2.  `!git configure --local user.email "oub@mat.ucm.es"`

y ya está. Comprobamos:

1.  editamos el fichero
2.  hacemos un commit y comprobamos

![img](./images/git-matlab41.png "Editar")

**TODO BIEN**

**¡OJO!** esa configuración solo es local para ese repositorio localmente clonado para tu ordenador en casa. Conviene configurarlo global para cualquier repositorio clonado. Esa configuración global no se puede hacer en los ordenadores del laboratorio.


<a id="org6fe09c2"></a>

### En el ordenador/portátil    individual (cambio global)

La diferencia es ahora:

1.  `!git configure --global user.name "Uwe Brauer"`
2.  `!git configure --gloabl user.email "oub@mat.ucm.es"`


<a id="org8ab1690"></a>

## Cambiar, editar ficheros .m con matlab. ¡OJO!

![img](./images/git-matlab18.png "Abrimos un fichero")

1.  Abrimos el fichero *annu<sub>hoja1</sub><sub>matlab</sub><sub>text.m</sub>*
2.  Ponemos nuestro nombre
3.  Grabamos el fichero

![img](./images/git-matlab20.png "Poner tu nombre")

¡OJO!: ahora el fichero annu<sub>hoja1</sub><sub>matlab</sub><sub>text</sub> está en **azul**


<a id="org45d46ea"></a>

## Después de haber terminado el trabajo: hacer un commit con un mensaje «inteligente»

Significa que tenemos que registrar el cambio (hacer un commit) con un mensaje inteligente.

![img](./images/git-matlab21.png "Elegir commit")

![img](./images/git-matlab23.png "Poner un mensaje")

Presionamos commit, abajo del mensaje

Ahora todo está en verde.

![img](./images/git-matlab24.png "Resultado del commit")


<a id="org322b18d"></a>

## Comprobamos el grafo

Si notamos algo así:

![img](./images/git-matlab39.png "Grafo")

tenemos un problema, porque en el ordenador de las aulas hay una configuración global de nombre y email y usa git.

Así que tenemos que configurar nuestro repositorio en el ordenador en la UCM. Como no es seguro que vayáis a usar siempre el mismo ordenador en las aulas, es conveniente hacer eso cada vez antes de empezar a trabajar, ¡¡¡incluso en casa!!!


<a id="org6ebf89d"></a>

## Qué hacer si `!git` falla

En el caso de que `!git` no funcione en matlab, tenemos que editar el fichero `config` dentro del directorio no visible .git. Los pasos son los siguientes:

-   en la línea de comandos de matlab escribir `cd .git` y sale la siguiente gráfica

![img](./images/git-matlab43.png "In el directorio .git")

f![img](./images/git-matlab44.png "Fichero de configuración")

-   Así que elegimos abrir el fichero config como fichero de texto

![img](./images/git-matlab45.png "Abrir como fichero de texto")

-   y sale

![img](./images/git-matlab46.png "El fichero de configuración")

-   añadinos nombre y email así

![img](./images/git-matlab47.png "Nombre y email")


<a id="org6b69e95"></a>

## Hacer un push a su fork en gitlab

Presionamos push

![img](./images/git-matlab25.png "Hacer un push")

Otra vez sale clave y usuario.

![img](./images/git-matlab26.png "Poner clave y usuario")


<a id="org2f09f43"></a>

## Comprobamos con el navegador si todo está bien

![img](./images/git-matlab27.png "Comprobación en gitlab")


<a id="orgd1f1da9"></a>

## Miramos en commits (abajo la project ID)

![img](./images/git-matlab28.png "Comprobación de los commits")

y vemos

![img](./images/git-matlab29.png "Lista de los commits")


<a id="orgb68df1a"></a>

## Resolver los problemas con error 403 en la conexión

El problema es que en los ordenadores del laboratorio queda grabado el usuario y su del primer proceso de `clonear`

La solución es fácil:

En matlab, en la línea de comandos, se tiene que escribir:

`!git config --global credential.helper ""`

![img](./images/git-matlab50.png "desactivar credential helper")


<a id="org72f93da"></a>

# Como usar Git en clase durante el curso

1.  Es impresendible por lo menos hacer un commit durante la clase, mejor más commits.
2.  Es también muy importante escribir mensajes informativos en cada commit