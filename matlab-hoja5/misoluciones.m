% No Rigido

%% EulerimpFix

%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 1 -2]*[y(1,:);y(2,:)]+[2*sin(t);2*(cos(t)-sin(t))]];
jyprime=@(t,y)[[-2 1; 1 -2]];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[teuloub,yeuloub,ev]=mieulerimpfix(yprime,intv,y0,N,TOL,nmax);
yeulimp=yeuloub.'
%#+end_src

%#+RESULTS:
yeulimp =
    2.0000    3.0000
    1.8321    2.5555
    1.7261    2.1350
    1.6542    1.7274
    1.5929    1.3284
    1.5237    0.9395
    1.4328    0.5660
    1.3113    0.2165
    1.1552   -0.0994
    0.9648   -0.3714
    0.7445   -0.5902
    0.5018   -0.7481
    0.2472   -0.8402
   -0.0074   -0.8647
   -0.2491   -0.8231
   -0.4656   -0.7204
   -0.6452   -0.5651
   -0.7783   -0.3683
   -0.8581   -0.1436
   -0.8804    0.0941
   -0.8448    0.3292
   -0.7540    0.5464
   -0.6143    0.7317
   -0.4348    0.8732
   -0.2269    0.9616
   -0.0038    0.9914
    0.2203    0.9604
    0.4315    0.8703
    0.6163    0.7267
    0.7633    0.5383
    0.8631    0.3168
    0.9096    0.0758
    0.8997   -0.1696
    0.8341   -0.4043
    0.7167   -0.6137
    0.5556   -0.7855
    0.3591   -0.9075
    0.1405   -0.9731
   -0.0867   -0.9783
   -0.3084   -0.9226
   -0.5109   -0.8095



%% EulerimpNwt
%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 1 -2]*[y(1,:);y(2,:)]+[2*sin(t);2*(cos(t)-sin(t))]];
jyprime=@(t,y)[[-2 1; 1 -2]];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[tnwtoub, yeulnwtoub,ev,loopcount]=mieulerimpnwt(yprime,jyprime,intv,y0,N,TOL,nmax);
yeulNwt=yeulnwtoub.'
%#+end_src

%#+RESULTS:
yeulNwt =
    2.0000    3.0000
    1.8403    2.5472
    1.7402    2.1209
    1.6722    1.7093
    1.6133    1.3080
    1.5449    0.9183
    1.4534    0.5455
    1.3299    0.1979
    1.1706   -0.1148
    0.9760   -0.3826
    0.7507   -0.5965
    0.5028   -0.7492
    0.2429   -0.8360
   -0.0167   -0.8554
   -0.2629   -0.8093
   -0.4830   -0.7031
   -0.6651   -0.5452
   -0.7996   -0.3471
   -0.8793   -0.1224
   -0.9003    0.1140
   -0.8621    0.3465
   -0.7677    0.5601
   -0.6235    0.7409
   -0.4389    0.8773
   -0.2257    0.9604
    0.0027    0.9849
    0.2317    0.9491
    0.4469    0.8548
    0.6350    0.7080
    0.7839    0.5176
    0.8845    0.2954
    0.9304    0.0550
    0.9186   -0.1885
    0.8499   -0.4201
    0.7285   -0.6255
    0.5620   -0.7919
    0.3605   -0.9089
    0.1368   -0.9694
   -0.0955   -0.9695
   -0.3217   -0.9093
   -0.5279   -0.7925



%% TrapFix

%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 1 -2]*[y(1,:);y(2,:)]+[2*sin(t);2*(cos(t)-sin(t))]];
jyprime=@(t,y)[[-2 1; 1 -2]];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[ttrap,ytrap,ev,loopcount]=mitrapfix(yprime,intv,y0,N,TOL,nmax);
ytrpfix=ytrap.'
%#+end_src

%#+RESULTS:

ytrpfix =

    2.0000    3.0000
    1.8021    2.5243
    1.6880    2.0874
    1.6212    1.6728
    1.5721    1.2726
    1.5173    0.8853
    1.4399    0.5147
    1.3286    0.1677
    1.1777   -0.1460
    0.9877   -0.4173
    0.7626   -0.6365
    0.5105   -0.7959
    0.2426   -0.8899
   -0.0286   -0.9158
   -0.2882   -0.8753
   -0.5224   -0.7730
   -0.7183   -0.6168
   -0.8650   -0.4178
   -0.9544   -0.1893
   -0.9817    0.0537
   -0.9459    0.2955
   -0.8493    0.5203
   -0.6992    0.7145
   -0.5041    0.8650
   -0.2774    0.9628
   -0.0325    1.0010
    0.2149    0.9775
    0.4493    0.8936
    0.6560    0.7545
    0.8221    0.5687
    0.9373    0.3476
    0.9944    0.1051
    0.9897   -0.1438
    0.9232   -0.3834
    0.7997   -0.5993
    0.6267   -0.7781
    0.4149   -0.9086
    0.1773   -0.9826
   -0.0718   -0.9950
   -0.3162   -0.9456
   -0.5410   -0.8375


%% TrapNwt

%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 1 -2]*[y(1,:);y(2,:)]+[2*sin(t);2*(cos(t)-sin(t))]];
jyprime=@(t,y)[[-2 1; 1 -2]];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[ttrapnwt,ytrapnwt]=mitrapnwt(yprime,jyprime,intv,y0,N,TOL,nmax);
ytrapNwt=ytrapnwt.'
%#+end_src

%#+RESULTS:

ytrapNwt =

    2.0000    3.0000
    1.8019    2.5245
    1.6877    2.0876
    1.6209    1.6731
    1.5718    1.2729
    1.5170    0.8856
    1.4396    0.5150
    1.3283    0.1680
    1.1778   -0.1461
    0.9878   -0.4175
    0.7626   -0.6366
    0.5103   -0.7958
    0.2422   -0.8896
   -0.0286   -0.9158
   -0.2881   -0.8755
   -0.5222   -0.7733
   -0.7180   -0.6171
   -0.8647   -0.4182
   -0.9541   -0.1896
   -0.9815    0.0534
   -0.9457    0.2952
   -0.8494    0.5203
   -0.6991    0.7144
   -0.5043    0.8650
   -0.2773    0.9627
   -0.0325    1.0011
    0.2147    0.9777
    0.4490    0.8939
    0.6557    0.7548
    0.8218    0.5690
    0.9370    0.3480
    0.9941    0.1054
    0.9894   -0.1435
    0.9233   -0.3835
    0.7999   -0.5995
    0.6268   -0.7783
    0.4148   -0.9086
    0.1770   -0.9823
   -0.0718   -0.9950
   -0.3161   -0.9458
   -0.5407   -0.8378




%%  Rigido

%% EulerimpFix

%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 998 -999]*[y(1,:);y(2,:)]+[2*sin(t);999*(cos(t)-sin(t))]];
jyprime=@(t,y)[-2 1; 008 -999];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[teuloub,yeuloub,ev]=mieulerimpfix(yprime,intv,y0,N,TOL,nmax);
yeulimp=yeuloub.'
%#+end_src

%#+RESULTS:

yeulimp =

  1.0e+287 *

    0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0015    1.5075
      -Inf       Inf
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN




%% EulerimpNwt
%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 998 -999]*[y(1,:);y(2,:)]+[2*sin(t);999*(cos(t)-sin(t))]];
jyprime=@(t,y)[-2 1; 008 -999];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[tnwtoub, yeulnwtoub,ev,loopcount]=mieulerimpnwt(yprime,jyprime,intv,y0,N,TOL,nmax);
yeulNwt=yeulnwtoub.'
%#+end_src

%#+RESULTS:

yeulNwt =

    2.0000    3.0000
    1.8432    2.5646
    1.7460    2.1441
    1.6794    1.7295
    1.6199    1.3188
    1.5488    0.9152
    1.4527    0.5261
    1.3233    0.1613
    1.1573   -0.1679
    0.9559   -0.4502
    0.7242   -0.6752
    0.4709   -0.8349
    0.2070   -0.9240
   -0.0549   -0.9407
   -0.3013   -0.8869
   -0.5195   -0.7684
   -0.6977   -0.5945
   -0.8264   -0.3775
   -0.8988   -0.1321
   -0.9114    0.1254
   -0.8642    0.3783
   -0.7608    0.6101
   -0.6080    0.8060
   -0.4159    0.9534
   -0.1966    1.0429
    0.0360    1.0685
    0.2671    1.0285
    0.4823    0.9252
    0.6680    0.7649
    0.8126    0.5575
    0.9070    0.3157
    0.9453    0.0546
    0.9250   -0.2097
    0.8474   -0.4608
    0.7173   -0.6830
    0.5426   -0.8628
    0.3343   -0.9887
    0.1053   -1.0532
   -0.1302   -1.0521
   -0.3575   -0.9855
   -0.5626   -0.8576


%% TrapFix

%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 998 -999]*[y(1,:);y(2,:)]+[2*sin(t);999*(cos(t)-sin(t))]];
jyprime=@(t,y)[-2 1; 008 -999];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[ttrap,ytrap,ev,loopcount]=mitrapfix(yprime,intv,y0,N,TOL,nmax);
ytrpfix=ytrap.'
%#+end_src

%#+RESULTS:

ytrpfix =

  1.0e+296 *

    0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0000    0.0000
   -0.0076    7.5886
      -Inf       Inf
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN
       NaN       NaN


%% TrapNwt

%#+begin_src matlab  :tangle annu_sol_ex23.m :padline yes :results output raw :exports results  :eval never-export 
intv=[0,10];                            % intervalo
yexact=@(t,y)[ [ 1; 1]*2*exp(-t)+[sin(t);cos(t)]];
yprime=@(t,y)[[-2 1; 998 -999]*[y(1,:);y(2,:)]+[2*sin(t);999*(cos(t)-sin(t))]];
jyprime=@(t,y)[-2 1; 008 -999];
y0=[2,3];
N=40; 
M=8;
TOL=0.001;
nmax=10;
[ttrapnwt,ytrapnwt]=mitrapnwt(yprime,jyprime,intv,y0,N,TOL,nmax);
ytrapNwt=ytrapnwt.'
%#+end_src

%#+RESULTS:

ytrapNwt =

    2.0000    3.0000
    1.8018    2.5234
    1.6873    2.0856
    1.6202    1.6702
    1.5707    1.2696
    1.5157    0.8820
    1.4381    0.5114
    1.3267    0.1645
    1.1762   -0.1492
    0.9863   -0.4199
    0.7613   -0.6384
    0.5093   -0.7967
    0.2415   -0.8896
   -0.0290   -0.9149
   -0.2880   -0.8737
   -0.5217   -0.7707
   -0.7172   -0.6140
   -0.8635   -0.4146
   -0.9527   -0.1859
   -0.9799    0.0570
   -0.9440    0.2986
   -0.8478    0.5231
   -0.6976    0.7167
   -0.5030    0.8664
   -0.2764    0.9633
   -0.0320    1.0006
    0.2149    0.9765
    0.4488    0.8917
    0.6551    0.7520
    0.8208    0.5656
    0.9357    0.3443
    0.9925    0.1017
    0.9878   -0.1470
    0.9217   -0.3867
    0.7984   -0.6021
    0.6254   -0.7801
    0.4137   -0.9096
    0.1762   -0.9825
   -0.0722   -0.9942
   -0.3161   -0.9442
   -0.5403   -0.8354


